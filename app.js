const express = require('express')
const app = express();

process.env.JWT_KEY = "JwtKeyToGenerateTheToken";

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const userController = require('./models/users/user-controller');
app.use('/users', userController);

const projectController = require('./models/projects/project-controller');
app.use('/projects', projectController);

module.exports = app;