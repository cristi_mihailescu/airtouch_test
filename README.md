# airtouch_test

> A simple backend test

### Project Structure
    .
    ├── common
    │   ├── authorize.js            # Authentication + Authorization JWT helper to parse Token and to verify if the user has the right roles
    │   └── role.js                 # Roles enum
    ├── models                      # feature-base models: services provide the data and run the business logic, controllers are building-up routes handlers
    │   ├── projects
    │   │   ├── project-controller.js
    │   │   └── project-service.js
    │   ├── users
    │   │   ├── user-controller.js
    │   │   └── user-service.js
    ├── test
    │   └── test.js                 # Mocha test specs + HTTP integration asserts with Chai
    ├── app.js                      # app built-up: express app configuration, JWT key value
    └── server.js                   # should start and listend the express app, if needed


### Endpoints

#### private

- GET  /users/profile/:id     # get a specific user's profile
- GET  /users/profiles        # *ADMIN ONLY* get all users' profiles
- PUT  /projects/add          # *ADMIN ONLY* creates a project

#### public

- POST /users/register        # create an user
- POST /users/login           # authenicate an user
- GET  /projects/get          # returns all projects


### Usage

    npm install
    npm test


It will run through some Mocha specs to assert Chai HTTP tests so that it will cover the functionality of the above endpoints:

#### User registration

- Should return 200 and confirmation for a new user
- Should return 400 and an error message for a user already registered

#### User login

- Should return 200 and a token for valid credentials
- Should return 400 and an error for invalid credentials

#### User profile

- Should return 200 and correct profile data for an existing user
- Should return 401 when the current user is not authorized
- Should return 401 and an error message when a non-admin tries to get other user profile
- Should return 404 when the profile doesnt exists
- Should return 200 and all profiles data for an admin user
- Should return 401 when a non-admin user tries to get the profiles

#### Projects

- Should return 200 and all profiles for a non-authenticated user
- Should return 200 and a confirmation message when a project is created by an admin
- Should return 401 when a non-admin tries to create a project


### What's not here yet:

- no real DB / ORM integration; still, it would be pretty easy to add a DDT layer such Sequelize in the *-service.js files
- no typings / interface usage / dependecy injections
- no unit testing, only HTTP integration testing
- no cookies based Token keeping - for now is passed as a request header parameter
- no require / format validation of the fields; we can use Joi for this
- no real HTTP server listening, all functionality is under testing