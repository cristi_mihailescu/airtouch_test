const chai = require('chai');
const expect = chai.expect;

const http = require('chai-http');
chai.use(http);
chai.should();

const app = require('../app');

describe('User registration', () => {

  it('Should return 200 and confirmation for a new user', (done) => {
    
    const newUser = {
      name: 'John Doe',
      username: 'johndoe',
      email: 'johndoe@airtouchmedia.com',
      password: 'secret',
      lastName: 'John',
      password: 'Doe'
    }
    
    chai.request(app)
      .post('/users/register')
      .send(newUser)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.message).to.be.equal("User created");

        done()
      })
  }),

  it('Should return 400 and an error message for a user already registered', (done) => {
    
    const newUser = {
      username: 'admin', 
      password: 'admin1', 
      email: 'admin1@airtouchmedia.com', 
      firstName: 'AdminFirstName2', 
      lastName: 'AdminLastName2'
    }
    
    chai.request(app)
      .post('/users/register')
      .send(newUser)
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res.body.message).to.be.equal("Username or password already exists");

        done()
      })
  })

});

describe('User login', () => {
  it('Should return 200 and a token for valid credentials', (done) => {

    const existingUser = {
      username: 'user1',
      password: 'user'
    }

    chai.request(app)
      .post('/users/login')
      .send(existingUser)
      .end((err, res) => {        
        expect(res).to.have.status(200)
        expect(res.body.user.token).to.exist
        expect(res.body.message).to.be.equal('Auth OK')

        done()
      })
  }),

  it('Should return 400 and an error for invalid credentials', (done) => {

    const existingUser = {
      username: 'user1123',
      password: 'user'
    }

    chai.request(app)
      .post('/users/login')
      .send(existingUser)
      .end((err, res) => {           
        expect(res).to.have.status(400)
        expect(res.body.user).to.not.exist
        expect(res.body.message).to.be.equal('Username or password is incorrect')

        done()
      })
  })
});

describe('User profile', () => {
  it('Should return 200 and correct profile data for an existing user', (done) => {

    const existingUser = {
      username: 'user1',
      password: 'user'
    }

    chai.request(app)
      .post('/users/login')
      .send(existingUser)
      .end((err, res) => {        
        expect(res).to.have.status(200)
        expect(res.body.user.token).to.exist
        expect(res.body.message).to.be.equal('Auth OK')

        chai.request(app)
          .get('/users/profile/2')
          .set({ token: res.body.user.token }) // set the jwt token as request header - we can get it from query or from cookies etc.
          .end((err, res) => {        
    
            expect(res).to.have.status(200)            
            expect(res.body.email).to.be.equal('user1@airtouchmedia.com')
    
            done()
          })

      })
    }),

  it('Should return 401 when the current user is not authorized', (done) => {

    chai.request(app)
      .get('/users/profile/1')
      
      // use a wrong token / user haven't logged in
      .set({ token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsInJvbGUiOiJVc2VyIiwiaWF0IjoxNTUxNDcyNTI0fQ.6b03y63votz1rBQddjJ2l20qX-CBk6AETirppk4YAy4' })
      .end((err, res) => {        
        expect(res).to.have.status(401)

        done()
      })
    }),

  it('Should return 401 and an error message when a non-admin tries to get other user profile', (done) => {

    const existingUser = {
      username: 'user1',
      password: 'user'
    }

    chai.request(app)
      .post('/users/login')
      .send(existingUser)
      .end((err, res) => {        

        expect(res).to.have.status(200)
        expect(res.body.user.token).to.exist
        expect(res.body.message).to.be.equal('Auth OK')

        chai.request(app)
          .get('/users/profile/1')
          .set({ token: res.body.user.token })
          .end((err, res) => {        
    
            expect(res).to.have.status(401)            
            expect(res.body.message).to.be.equal('Unauthorized Role')
    
            done()
          })

      })
    }),

  it('Should return 404 when the profile doesnt exists', (done) => {

    const existingUser = {
      username: 'admin',
      password: 'admin'
    }

    chai.request(app)
      .post('/users/login')
      .send(existingUser)
      .end((err, res) => {        
        expect(res).to.have.status(200)
        expect(res.body.user.token).to.exist
        expect(res.body.message).to.be.equal('Auth OK')

        chai.request(app)
          .get('/users/profile/33') // wrong userId
          .set({ token: res.body.user.token })
          .end((err, res) => {        
            expect(res).to.have.status(404)            
    
            done()
          })

      })
    }),

  it('Should return 200 and all profiles data for an admin user', (done) => {

    const existingUser = {
      username: 'admin',
      password: 'admin'
    }

    chai.request(app)
      .post('/users/login')
      .send(existingUser)
      .end((err, res) => {        
        expect(res).to.have.status(200)
        expect(res.body.user.token).to.exist
        expect(res.body.message).to.be.equal('Auth OK')

        chai.request(app)
          .get('/users/profiles')
          .set({ token: res.body.user.token }) 
          .end((err, res) => {        
    
            expect(res).to.have.status(200)   
            expect(res.body.length).to.be.equal(3) // should get all users' data
    
            done()
          })

      })
    }),

  it('Should return 401 when a non-admin user tries to get the profiles', (done) => {

    const existingUser = {
      username: 'user1',
      password: 'user'
    }

    chai.request(app)
      .post('/users/login')
      .send(existingUser)
      .end((err, res) => {        
        expect(res).to.have.status(200)
        expect(res.body.user.token).to.exist
        expect(res.body.message).to.be.equal('Auth OK')

        chai.request(app)
          .get('/users/profiles')
          .set({ token: res.body.user.token }) 
          .end((err, res) => {        
            expect(res).to.have.status(401)   
    
            done()
          })

      })
    })
}); 

describe('Projects', () => {

  it('Should return 200 and all profiles for a non-authenticated user', (done) => {

    chai.request(app)
      .get('/projects/get')
      .end((err, res) => {        

        expect(res).to.have.status(200)            
        expect(res.body.length).to.be.equal(1)

        done()
      })
    }),

  it('Should return 200 and a confirmation message when a project is created by an admin', (done) => {

    const newProject = {
      name: 'test project2',
      companyName: 'test company'
    }
    
    const existingUser = {
      username: 'admin',
      password: 'admin'
    }

    chai.request(app)
      .post('/users/login')
      .send(existingUser)
      .end((err, res) => {        
        expect(res).to.have.status(200)
        expect(res.body.user.token).to.exist
        expect(res.body.message).to.be.equal('Auth OK')

        chai.request(app)
          .put('/projects/add')
          .set({ token: res.body.user.token }) 
          .send(newProject)
          .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.body.message).to.be.equal("Project created");
    
            done()
          })
        })
    }),

  it('Should return 401 when a non-admin tries to create a project', (done) => {
  
    const newProject = {
      name: 'test project2',
      companyName: 'test company'
    }
    
    const existingUser = {
      username: 'user1',
      password: 'user'
    }

    chai.request(app)
      .post('/users/login')
      .send(existingUser)
      .end((err, res) => {        
        expect(res).to.have.status(200)
        expect(res.body.user.token).to.exist
        expect(res.body.message).to.be.equal('Auth OK')

        chai.request(app)
          .put('/projects/add')
          .set({ token: res.body.user.token }) 
          .send(newProject)
          .end((err, res) => {
            expect(res).to.have.status(401);
    
            done()
          })
        })
    })

});