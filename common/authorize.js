const expressJwt = require('express-jwt');

function authorize(roles = []) { // one or more Roles
    
    if (typeof roles === 'string') {
        roles = [roles];
    }

    return [
        // authenticate JWT token; it will attach the user to req
        expressJwt({ secret: process.env.JWT_KEY,
            getToken: function (req) {
                if (req.headers.token) { // try to get it from headers                  
                  return req.headers.token
                }
            
                // no Token found, unauthorized (401)
                return null; 
              } }),
       
        (req, res, next) => {
            if (roles.length && !roles.includes(req.user.role)) {                
                return res.status(401).json({ message: 'Unauthorized' });
            }

            // authentication & authorization successful
            next();
        }
    ];
}

module.exports = authorize;