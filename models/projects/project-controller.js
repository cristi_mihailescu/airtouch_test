const express = require('express');
const router = express.Router();

const projectService = require('./project-service.js');
const authorize = require('../../common/authorize')
const Role = require('../../common/role');

// routes
// PUBLIC
router.get('/get', getProjects);

// RESTRICTED
router.put('/add', authorize(Role.Admin), addProject); // admin only

function getProjects(req, res, next) {
    projectService.getAll()
        .then(projects => res.json(projects))
        .catch(err => next(err));
}

function addProject(req, res, next) {

    const currentUser = req.user;

    // only admins can create projects
    if (currentUser.role !== Role.Admin) {
        return res.status(401).json({ message: 'Unauthorized Role' });
    }

    projectService.add(req.body);
    res.status(200).json({ message: 'Project created'});
}

module.exports = router;