let projects = [
    { id: 1, name: 'test project', companyName: 'Air Touch Media' }
];

let lastProjectId = 1;

async function add({ name, companyName }){
    projects.push({ id: ++lastProjectId, name: name, companyName: companyName }); 
}

async function getAll() {
    return projects.map(u => {
        return { name: u.name, companyName: u.companyName };
    });
}

module.exports = {
    add,
    getAll
};