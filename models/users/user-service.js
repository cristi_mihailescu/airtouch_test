const jwt = require('jsonwebtoken');
const Role = require('../../common/role');

// some default users, on different roles
let users = [
    { id: 1, username: 'admin', password: 'admin', email: 'admin@airtouchmedia.com', firstName: 'AdminFirstName', lastName: 'AdminLastName', role: Role.Admin },
    { id: 2, username: 'user1', password: 'user', email: 'user1@airtouchmedia.com', firstName: 'User1FirstName', lastName: 'User1LastName', role: Role.User }
];

let lastUserId = 2;

async function register({ username, password, email, firstName, lastName }){
    const user = users.find(u => u.username === username || u.email === email);
    if (!user) {
        users.push({ id: ++lastUserId, username: username, email: email, password: password, 
            firstName: firstName, lastName: lastName, role: Role.User });  

        return true;
    } 
    return false;  
}

async function login({ username, password }) {
    const user = users.find(u => u.username === username && u.password === password);
    if (user) {
        const token = jwt.sign({ sub: user.id, role: user.role }, process.env.JWT_KEY);
        const { password, ...userProfile } = user;
        return {
            ...userProfile,
            token
        };
    }
}

async function getAll() {
    return users.map(u => {
        const { password, ...userProfile } = u;
        return userProfile;
    });
}

async function getProfileById(id) {
    const user = users.find(u => u.id === parseInt(id));
    if (!user) 
        return;
    const { password, ...userProfile } = user;
    return userProfile;
}

module.exports = {
    register,
    login,
    getAll,
    getProfileById
};