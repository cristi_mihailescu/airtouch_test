const express = require('express');
const router = express.Router();

const userService = require('./user-service.js');
const authorize = require('../../common/authorize')
const Role = require('../../common/role');


// routes
// PUBLIC
router.post('/register', register);
router.post('/login', login);

// RESTRICTED
router.get('/profile/:id', authorize(), getProfileById);  // can be accessed by all valid users
router.get('/profiles', authorize(Role.Admin), getAll); // admin only

function register(req, res, next) {
    userService.register(req.body)
        .then(userCreated => userCreated ? res.json({ message: 'User created' }) 
            : res.status(400).json({ message: 'Username or password already exists' }))
        .catch(err => next(err));
}

function login(req, res, next) {
    userService.login(req.body)
        .then(user => user ? res.json({ user: user, message: 'Auth OK'}) 
            : res.status(400).json({ message: 'Username or password is incorrect' }))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    userService.getAll()
        .then(users => res.json(users))
        .catch(err => next(err));
}

function getProfileById(req, res, next) {

    const currentUser = req.user;
    const id = parseInt(req.params.id);

    // only admins can access other user's data!
    if (id !== currentUser.sub && currentUser.role !== Role.Admin) {
        return res.status(401).json({ message: 'Unauthorized Role' });
    }

    userService.getProfileById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

module.exports = router;